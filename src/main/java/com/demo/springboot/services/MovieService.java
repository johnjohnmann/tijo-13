package com.demo.springboot.services;

import com.demo.springboot.domain.dto.MovieDto;

import java.util.List;

public interface MovieService {
    List<MovieDto> readMovies();

    boolean addMovie(MovieDto movie);
}
