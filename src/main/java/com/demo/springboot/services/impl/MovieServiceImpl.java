package com.demo.springboot.services.impl;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.services.MovieService;
import com.opencsv.*;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MovieServiceImpl implements MovieService {
    private String fileName = "src/main/resources/movies.csv";

    @Override
    public List<MovieDto> readMovies() {
        List<MovieDto> movies = new ArrayList<>();

        CSVParser parser = new CSVParserBuilder()
                .withSeparator(';')
                .withIgnoreQuotations(true)
                .build();

        try (CSVReader csvReader = new CSVReaderBuilder(new FileReader(fileName))
                .withSkipLines(1)
                .withCSVParser(parser)
                .build()) {

            String[] line;
            while ((line = csvReader.readNext()) != null) {
                movies.add(new MovieDto(
                        Integer.parseInt(line[0].trim()),
                        line[1].trim(),
                        Integer.parseInt(line[2].trim()),
                        line[3].trim()
                ));
            }
        } catch (IOException error) {
            System.err.println("IOException: ");
            error.printStackTrace();
        }

        return movies;
    }

    @Override
    public boolean addMovie(MovieDto movieToAdd) {
        List<MovieDto> movies = readMovies();
        for(MovieDto movie: movies) {
            if(movie.getMovieId() == movieToAdd.getMovieId()) {
                return false;
            }
        }

        try (CSVWriter writer = new CSVWriter(
                new FileWriter(fileName, true),
                ';',
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                "\r\n")){

            String[] data = {
                    Integer.toString(movieToAdd.getMovieId()),
                    movieToAdd.getTitle(),
                    Integer.toString(movieToAdd.getYear()),
                    movieToAdd.getImage()
            };
            writer.writeNext(data);
        } catch (IOException error) {
            System.out.println("addMovie IOException!");
        }
        return true;
    }
}
