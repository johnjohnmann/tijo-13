package com.demo.springboot.rest;

import com.demo.springboot.domain.dto.MovieDto;
import com.demo.springboot.domain.dto.MovieListDto;
import com.demo.springboot.services.MovieService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class MovieApiController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MovieApiController.class);

    @Autowired
    private MovieService movieService;

    @RequestMapping(value = "/movies", method = RequestMethod.GET)
    @ResponseBody
    @CrossOrigin
    public ResponseEntity<MovieListDto> getMovies() {

        LOGGER.info("--- get movies");

        List<MovieDto> movies = movieService.readMovies();

        return new ResponseEntity<>(new MovieListDto(movies), HttpStatus.OK);
    }

    @RequestMapping(value = "/movies", method = RequestMethod.POST)
    @CrossOrigin
    public ResponseEntity<?> createMovie(@RequestBody MovieDto movieDto) {

        LOGGER.info("--- add movie");

        if(movieService.addMovie(movieDto)) {
            return new ResponseEntity<>(HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.CONFLICT);
    }
}
